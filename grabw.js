/*
MIT License

Copyright (c) 2023 Radim Kolar

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice (including the next paragraph) shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';
import { URL } from 'url';

import { grab, guessTarget } from './lib/grab.js';

function parseCLI(args) {
  const { argv } = yargs(hideBin(args))
    .command('$0 <url> [target]', 'Download a page', (yargs) => {
      yargs
        .positional('url', {
          type: 'string',
          describe: 'URL to download',
          coerce: (url) => {
            try {
              return new URL(url).href;
            } catch (err) {
              throw new Error('Invalid URL');
            }
          },
        })
        .positional('target', {
          type: 'string',
          describe: 'Target directory',
        });
    });

  return [argv.url, argv.target || guessTarget(argv.url)];
}

function acceptURL(url) {
  const audioExtensions = ['mp3', 'ogg', 'wav', 'flac', 'aif', 'aiff', 'rar', 'zip'];
  const extension = url.split('.').pop();
  return url.startsWith(start) && audioExtensions.includes(extension);
}

/* main program */

const [start, target] = parseCLI(process.argv);
console.log(start, target);

await grab(start, target, acceptURL)