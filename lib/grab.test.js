import test from 'ava';
import { guessTarget } from './grab.js';

test('guessTarget url slash', t => {
   const rc = guessTarget('https://www.autistici.org/2000-maniax/samples/')
   t.is('samples', rc)
});

test('guessTarget url, no slash', t => {
   const rc = guessTarget('https://www.autistici.org/2000-maniax/samples')
   t.is('samples', rc)
});

test('guessTarget site', t => {
   const rc = guessTarget('https://www.autistici.org/')
   t.is('www.autistici.org', rc)
});

test('guessTarget site, no slash', t => {
   const rc = guessTarget('https://www.autistici.org')
   t.is('www.autistici.org', rc)
});


