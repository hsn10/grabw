import cheerio from 'cheerio';
import { downloadData, downloadPage } from './net.js';
import fs from 'fs';

/**
Guesses target directory from URL.
Last directory component of URL is used
*/
function guessTarget(url) {
  const urlParts = url.split('/');
  return urlParts[urlParts.length - 1] || urlParts[urlParts.length - 2];
}

/**
  Recursively download page starting at source to local directory target
*/
async function grab(source, target, accept) {
  // Download the page
  const page = await downloadPage(source);
  console.log(page);

  // Load the page into cheerio
  const $ = cheerio.load(page);

  // Find all links on the page
  $('a').each((i, link) => {
    // extracted link from page
    const href = $(link).attr('href');
    // Construct the full URL
    const url = new URL(href, source).href;

    // Check if the link is a directory
    if (href.endsWith('/') && url.startsWith(source) ) {
        // Recursively download the directory
        grab(url, `${target}/${decodeURIComponent(href)}`, accept);
    } else {

    // Check if the link should be accepted
    if (accept(url)) {
        // Download the file
        downloadData(url).then((data) => {
          // Check if the target directory exists
          if (!fs.existsSync(target)) {
            // Create the target directory
            fs.mkdirSync(target, { recursive: true });
          }
          // Save the file to the target directory
          fs.writeFileSync(`${target}/${decodeURIComponent(href)}`, data);
        });
      }
    }
  });
}

export default grab;
export { grab, guessTarget };