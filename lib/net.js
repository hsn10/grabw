import https from 'https';
import http from 'http';

/**
  Downloads page using http or https returning Promise containing String
*/
function downloadPage(url) {
  const protocol = url.startsWith('https') ? https : http;

  return new Promise((resolve, reject) => {
    protocol.get(url, (res) => {
      let data = '';

      res.on('data', (chunk) => {
        data += chunk;
      });

      res.on('end', () => {
        resolve(data);
      });

      res.on('error', (err) => {
        reject(err);
      });
    });
  });
}

/**
Downloads binary data from URL
*/
function downloadData(url) {
  const protocol = url.startsWith('https') ? https : http;

  return new Promise((resolve, reject) => {
    protocol.get(url, (res) => {
      const data = [];

      res.on('data', (chunk) => {
        data.push(chunk);
      });

      res.on('end', () => {
        resolve(Buffer.concat(data));
      });

      res.on('error', (err) => {
        reject(err);
      });
    });
  });
}

export { downloadPage, downloadData }
